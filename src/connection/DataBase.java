/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connection;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import model.Log;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Naturals03
 */
public class DataBase {

    private List<String> values;
    private String url = "";
    private String usuario = "";
    private String senha = "";
    private String tabela = "";

    private String driver = "org.postgresql.Driver";
    Connection con;

    public DataBase(List<String> values) {
        this.values = values;
        if (values.size() != 0) {
            this.url = "jdbc:" + values.get(0) + "://" + values.get(1) + ":" + values.get(3) + "/" + values.get(2);
            this.usuario = values.get(4);
            this.senha = values.get(5);
            this.tabela = values.get(6);
        }
    }

    public Connection Conect() {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException cnfe) {
            cnfe.printStackTrace();
        }
        try {
            con = DriverManager.getConnection(url, usuario, senha);
        } catch (SQLException se) {
            System.out.println(se.getMessage());
            se.printStackTrace();
        }
        return con;
    }

    public Boolean isConected() {
        try {
            Connection bd = new connection.DataBase(values).Conect();
            bd.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public Boolean existTable(){
        try {
            Connection bd = new connection.DataBase(values).Conect();
            DatabaseMetaData dbm = bd.getMetaData();
            ResultSet tables = dbm.getTables(null, null, values.get(6), null);
            bd.close();
            Boolean isTable = tables.next();
            tables.close();
            return isTable;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public Boolean createDataBase(){
        Connection db = Conect();
        Statement statement = null;
        try {
            String sqlTable = "CREATE DATABASE "+values.get(2);
            statement = db.createStatement();
            statement.executeUpdate(sqlTable);
            statement.close();
            db.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    public Boolean createTable(){
        Connection db = Conect();
        Statement statement = null;
        try {
            String sqlTable = "CREATE SEQUENCE seqid"+this.tabela+""+
            "  INCREMENT 1" +
            "  MINVALUE 1" +
            "  MAXVALUE 9223372036854775807" +
            "  START 12090" +
            "  CACHE 1;"
            + "CREATE TABLE "+this.tabela+"(" +
            "  projeto character varying(255)," +
            "  classe character varying(255)," +
            "  linha integer," +
            "  caminho character varying(255)," +
            "  tipoerro character varying(100)," +
            "  atividade character varying(255)," +
            "  codigo character varying(6)," +
            "  excecao character varying," +
            "  id integer NOT NULL DEFAULT nextval('seqid"+this.tabela+"'::regclass)," +
            "  idusuario integer," +
            "  nomearquivo character varying(15)," +
            "  data bigint," +
            "  datacadastro bigint," +
            "  CONSTRAINT chave_primaria_"+this.tabela+" PRIMARY KEY (id))" +
            "WITH (OIDS=FALSE);";
            statement = db.createStatement();
            statement.execute(sqlTable);
            statement.close();
            db.close();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void insert(List<Log> listLogs) throws SQLException {
        Connection db = Conect();
        try {
            String sql = "insert into " + this.tabela + " (projeto, classe, linha, caminho, tipoerro, atividade, codigo, data, excecao, idusuario, nomearquivo, datacadastro)values(?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement pstm = db.prepareStatement(sql);
            db.setAutoCommit(false);
            for (Log log : listLogs) {
                pstm.setString(1, log.getProjeto());
                pstm.setString(2, log.getClasse());
                pstm.setInt(3, log.getLinha());
                pstm.setString(4, log.getCaminho());
                pstm.setString(5, log.getTipoErro());
                pstm.setString(6, log.getAtividade());
                pstm.setString(7, log.getCodigo());
                pstm.setLong(8, log.getData());
                pstm.setString(9, log.getExcecao());
                pstm.setInt(10, log.getIdUsuario());
                pstm.setString(11, log.getNomeArquivo());
                pstm.setLong(12, log.getDataCadastro());
                pstm.executeUpdate();
                db.commit();
            }
            pstm.close();
            db.close();
        } catch (SQLException e) {
            db.rollback();
            e.printStackTrace();
        }
    }
    public List<Log> getListModel(String pathFile) {
        ArrayList<Log> listLogModel = new ArrayList<Log>();
        BufferedReader br = null;
        JSONParser parser = new JSONParser();
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader(pathFile));
            while ((sCurrentLine = br.readLine()) != null) {
                Object obj;
                obj = parser.parse(sCurrentLine);
                JSONObject jsonObject = (JSONObject) obj;
                Log log = new Log();
                log.setLinha(Integer.parseInt(jsonObject.get("Line").toString()));
                log.setCaminho((String) jsonObject.get("Path"));
                log.setProjeto((String) jsonObject.get("Project"));
                log.setTipoErro((String) jsonObject.get("Type"));
                log.setAtividade((String) jsonObject.get("Message"));
                log.setIdUsuario(Integer.parseInt(jsonObject.get("UserID").toString()));
                log.setClasse((String) jsonObject.get("Class"));
                log.setCodigo((String) jsonObject.get("Code"));
                log.setData(Long.parseLong((String) jsonObject.get("Date")));
                log.setExcecao((String) jsonObject.get("Exception"));
                log.setNomeArquivo((new File(pathFile).getName()));
                log.setDataCadastro(new Date().getTime());
                listLogModel.add(log);
            }
            return listLogModel;
        } catch (IOException | ParseException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "O arquivo de Log não está em uma estrutura correta!");
        }
        return new ArrayList<Log>();
    }
}
