/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicativo;

import connection.DataBase;
import java.awt.HeadlessException;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import model.Log;

/**
 *
 * @author Marcos
 */
public class Window extends javax.swing.JFrame {

    public static final String nameFileConfig = "config.cnf";
    public List<String> pathsLogs = new ArrayList<>();

    /**
     * Creates new form Window
     */
    public Window() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtPath = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnIniciar = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtPaths = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        txtPath.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtPathMouseClicked(evt);
            }
        });
        txtPath.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPathActionPerformed(evt);
            }
        });

        jLabel1.setText("Selecione a Pasta de Logs:");

        btnIniciar.setText("Iniciar Extração");
        btnIniciar.setEnabled(false);
        btnIniciar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnIniciarActionPerformed(evt);
            }
        });

        txtPaths.setEditable(false);
        txtPaths.setColumns(20);
        txtPaths.setRows(5);
        jScrollPane3.setViewportView(txtPaths);

        jMenu1.setText("Opções");

        jMenuItem1.setText("Configurar Banco");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuItem1MouseClicked(evt);
            }
        });
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Sair");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Sobre");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE)
                    .addComponent(txtPath)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(144, 144, 144)
                .addComponent(btnIniciar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(2, 2, 2)
                .addComponent(txtPath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnIniciar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap(59, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        abrirDialog();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    public void abrirDialog() {
        Dialog janela = new Dialog(this, true);
        janela.setTitle("Configurar Conexão");
        janela.setLocationRelativeTo(null);
        janela.show();
    }
    private void txtPathActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPathActionPerformed

    }//GEN-LAST:event_txtPathActionPerformed

    private void txtPathMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtPathMouseClicked
        if (!(new File((new File("").getAbsoluteFile()) + "\\" + nameFileConfig)).exists())
            JOptionPane.showMessageDialog(null, "Ainda não existe um arquivo de Configuração. Para criar, acesse o menu Opções e em Configurar Banco!", "Atenção", JOptionPane.WARNING_MESSAGE);
        else {
            DataBase db = new DataBase(Dialog.getValuesConfig(nameFileConfig));
            if (db.isConected() && db.existTable()) {
                pathsLogs = new ArrayList<>();
                JFileChooser chooser = new JFileChooser();
                chooser.setCurrentDirectory(new java.io.File("."));
                chooser.setDialogTitle("Selecione um Diretório que contenha as Pastas com Logs");
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setAcceptAllFileFilterUsed(false);
                if (chooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                    File file = new File(chooser.getSelectedFile().toString());
                    File afile[] = file.listFiles();
                    int i = 0;
                    String pastas = "";
                    int totalLogs = 0;
                    for (int j = afile.length; i < j; i++) {
                        File arquivos = afile[i];
                        if (arquivos.isDirectory()) {
                            totalLogs += getTotalLogs(arquivos.getPath());
                            pastas += arquivos.getName() + " (Total de Logs: " + getTotalLogs(arquivos.getPath()) + ")\n";
                            if (totalLogs > 0)
                                pathsLogs.add(arquivos.getAbsolutePath());
                        }
                    }
                    if (totalLogs > 0){
                        btnIniciar.setEnabled(true);
                    }else
                        btnIniciar.setEnabled(false);
                    txtPaths.setText(pastas);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Os parâmetros do Banco de Configuração não estão corretos, favor verificar!", "Conexão ao Banco", JOptionPane.ERROR_MESSAGE);
                abrirDialog();
            }
        }
    }//GEN-LAST:event_txtPathMouseClicked

    private void btnIniciarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnIniciarActionPerformed
        try {
            for (String path : pathsLogs) {
                File file = new File(path);
                File afile[] = file.listFiles();
                int i = 0;
                for (int j = afile.length; i < j; i++) {
                    File arquivos = afile[i];
                    if (arquivos.isFile() && arquivos.toString().contains(".log")) {
                        DataBase db = new DataBase(Dialog.getValuesConfig(nameFileConfig));
                        List<Log> listLogs = db.getListModel(arquivos.getAbsolutePath());
                        db.insert(listLogs);
                    }
                    btnIniciar.setText("Extraindo logs...");
                }
            }
            JOptionPane.showMessageDialog(null, "Logs inseridos no Banco de Dados com Sucesso!");
            txtPaths.setText("");
            btnIniciar.setEnabled(false);
            btnIniciar.setText("Iniciar Extração");
        } catch (HeadlessException | SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Houve um erro ao Inserir o Registro!");
        }
    }//GEN-LAST:event_btnIniciarActionPerformed

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed

    }//GEN-LAST:event_jMenu2ActionPerformed

    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
        Sobre janela = new Sobre(this, true);
        janela.setTitle("Sobre");
        janela.setLocationRelativeTo(null);
        janela.setResizable(false);
        janela.show();
    }//GEN-LAST:event_jMenu2MouseClicked

    private void jMenuItem1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MouseClicked
        abrirDialog();
    }//GEN-LAST:event_jMenuItem1MouseClicked
    public Integer getTotalLogs(String path) {
        File file = new File(path);
        File afile[] = file.listFiles();
        int i = 0, total = 0;
        for (int j = afile.length; i < j; i++) {
            File arquivos = afile[i];
            if (arquivos.isFile() && arquivos.toString().contains(".log"))
                total++;
        }
        return total;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnIniciar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextField txtPath;
    private javax.swing.JTextArea txtPaths;
    // End of variables declaration//GEN-END:variables
}
