/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aplicativo;

import static aplicativo.Window.nameFileConfig;
import connection.DataBase;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author marko_000
 */
public class Dialog extends javax.swing.JDialog {

    /**
     * Creates new form Dialog
     *
     * @param parent
     * @param modal
     */
    public Dialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        try {
            checkBank.setEnabled(false);
            checkTable.setEnabled(false);
            File diretorio = new File("");
            File f = new File(diretorio.getAbsolutePath() + "\\" + nameFileConfig);
            if (!f.exists()) {
                JOptionPane.showMessageDialog(null, "O arquivo de configuração ainda não foi criado. Na próxima janela, favor fornecer os dados para Conexão!", "Atenção", JOptionPane.WARNING_MESSAGE);
                try {
                    f.createNewFile();
                    writeFile(f.getAbsoluteFile().toString(), null);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage());
                }
            } else {
                ArrayList<String> valuesConfig = (ArrayList<String>) getValuesConfig(nameFileConfig);
                if (valuesConfig.size() != 0) {
                    comboBoxBanco.setSelectedItem(valuesConfig == null ? "" : valuesConfig.get(0));
                    txtServidor.setText(valuesConfig == null ? "" : valuesConfig.get(1));
                    txtNomeBanco.setText(valuesConfig == null ? "" : valuesConfig.get(2));
                    txtPorta.setText(valuesConfig == null ? "" : valuesConfig.get(3));
                    txtUsuario.setText(valuesConfig == null ? "" : valuesConfig.get(4));
                    txtSenha.setText(valuesConfig == null ? "" : valuesConfig.get(5));
                    txtTabela.setText(valuesConfig == null ? "" : valuesConfig.get(6));
                    enableChecks();
                } else {
                    JOptionPane.showMessageDialog(null, "Não foi encontrado informações válidas no arquivo de Configuração!", "Atenção", JOptionPane.WARNING_MESSAGE);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
        }

    }

    public void enableChecks() {
        DataBase bd = new DataBase(getValuesConfig(nameFileConfig));
        if (bd.isConected()) {
            checkBank.setText("Conexão com Banco: OK!");
            checkBank.setSelected(true);
            if (!bd.existTable()) {
                checkTable.setText("Verificação de Tabela: ERRO!");
                checkTable.setSelected(false);
                int op = JOptionPane.showConfirmDialog(null, "Não foi possível identificar a existência da Tabela! Deseja Criar?", "Erro!", JOptionPane.ERROR_MESSAGE);
                if (op == 0)
                    if (bd.createTable())
                        JOptionPane.showMessageDialog(null, "Tabela criada com sucesso! Clique em Salvar para testar a conexão!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                     else 
                        JOptionPane.showMessageDialog(null, "Houve um problema ao tentar criar a Tabela!", "Erro!", JOptionPane.ERROR_MESSAGE);
            } else {
                checkTable.setText("Verificação de Tabela: OK!");
                checkTable.setSelected(true);
                JOptionPane.showMessageDialog(null, "Conexão realizada com Sucesso!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            checkBank.setText("Conexão com Banco: ERRO!");
            checkBank.setSelected(false);
            checkTable.setText("Verificação de Tabela: ERRO!");
            checkTable.setSelected(false);
            int op2 = JOptionPane.showConfirmDialog(null, "Não foi possível identificar a existência do Banco! Deseja Criar o Banco e a Tabela de acordo com as configurações?", "Erro!", JOptionPane.ERROR_MESSAGE);
            if (op2 == 0)
                if (bd.createDataBase()){
                    JOptionPane.showMessageDialog(null, "Banco criado com sucesso! Clique em Salvar para testar a conexão!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                    if(bd.createTable())
                        JOptionPane.showMessageDialog(null, "Tabela criada com sucesso! Clique em Salvar para testar a conexão!", "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
                    else
                        JOptionPane.showMessageDialog(null, "Houve um problema ao tentar criar a Tabela!", "Erro!", JOptionPane.ERROR_MESSAGE);
                }else 
                    JOptionPane.showMessageDialog(null, "Houve um problema ao tentar criar o Banco!", "Erro!", JOptionPane.ERROR_MESSAGE);
            else
                JOptionPane.showMessageDialog(null, "Não será possível realizar a transferência dos logs para um Banco!", "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        btnSalvar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtNomeBanco = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtPorta = new javax.swing.JTextField();
        comboBoxBanco = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        txtTabela = new javax.swing.JTextField();
        txtUsuario = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtServidor = new javax.swing.JTextField();
        txtSenha = new javax.swing.JPasswordField();
        checkTable = new javax.swing.JCheckBox();
        checkBank = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel4.setText("Nome da Tabela:");

        btnSalvar.setText("Salvar");
        btnSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalvarActionPerformed(evt);
            }
        });

        jLabel1.setText("Nome do Banco:");

        jLabel2.setText("Nº da Porta:");

        comboBoxBanco.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "postgresql" }));

        jLabel3.setText("Banco:");

        jLabel6.setText("Usuário do Banco:");

        jLabel7.setText("Senha do Banco:");

        jLabel8.setText("Servidor");

        checkTable.setText("Verificação de Tabela:");

        checkBank.setText("Conexão com Banco:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(txtSenha))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtTabela)
                            .addComponent(txtUsuario, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 186, Short.MAX_VALUE)
                                .addComponent(jLabel2))
                            .addComponent(txtServidor, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboBoxBanco, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addComponent(txtNomeBanco)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtPorta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(btnSalvar)
                        .addGap(21, 21, 21))
                    .addComponent(checkTable)
                    .addComponent(checkBank))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBoxBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtServidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNomeBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(1, 1, 1)
                .addComponent(txtSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtTabela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnSalvar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkBank)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkTable))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalvarActionPerformed
        ArrayList<String> valuesForms = new ArrayList<>();
        valuesForms.add(comboBoxBanco.getSelectedItem().toString());
        valuesForms.add(txtServidor.getText());
        valuesForms.add(txtNomeBanco.getText());
        valuesForms.add(txtPorta.getText());
        valuesForms.add(txtUsuario.getText());
        valuesForms.add(txtSenha.getText());
        valuesForms.add(txtTabela.getText());
        try {
            writeFile(nameFileConfig, valuesForms);
            enableChecks();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }//GEN-LAST:event_btnSalvarActionPerformed
    public static List<String> getValuesConfig(String path) {
        ArrayList<String> values = new ArrayList<>();
        if (!new File(path).exists()) {
            try {
                writeFile(path, null);
            } catch (IOException ex) {
                Logger.getLogger(Dialog.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JSONParser parser = new JSONParser();
            try {
                Object obj = parser.parse(new FileReader(path));
                JSONObject jsonObject = (JSONObject) obj;
                values.add((String) jsonObject.get("BancoConexao"));
                values.add((String) jsonObject.get("Servidor"));
                values.add((String) jsonObject.get("NomeBanco"));
                values.add((String) jsonObject.get("Porta"));
                values.add((String) jsonObject.get("Usuario"));
                values.add((String) jsonObject.get("Senha"));
                values.add((String) jsonObject.get("Tabela"));
                return values;
            } catch (IOException | ParseException ex) {
                Logger.getLogger(Dialog.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return new ArrayList<>();
    }

    public static void writeFile(String path, List<String> values) throws IOException {
        try {
            FileWriter config = new FileWriter(path);
            JSONObject obj = new JSONObject();
            obj.put("BancoConexao", values == null ? "" : values.get(0));
            obj.put("Servidor", values == null ? "" : values.get(1));
            obj.put("NomeBanco", values == null ? "" : values.get(2));
            obj.put("Porta", values == null ? "" : values.get(3));
            obj.put("Usuario", values == null ? "" : values.get(4));
            obj.put("Senha", values == null ? "" : values.get(5));
            obj.put("Tabela", values == null ? "" : values.get(6));
            config.write(obj.toJSONString());
            config.flush();
            config.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Erro!", e.getMessage(), JOptionPane.ERROR);
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSalvar;
    private javax.swing.JCheckBox checkBank;
    private javax.swing.JCheckBox checkTable;
    private javax.swing.JComboBox<String> comboBoxBanco;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField txtNomeBanco;
    private javax.swing.JTextField txtPorta;
    private javax.swing.JPasswordField txtSenha;
    private javax.swing.JTextField txtServidor;
    private javax.swing.JTextField txtTabela;
    private javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
